﻿

#include <iostream>
#include <string>

class Animals
{
private:

public:
    void virtual Voice()
    {
        std::string name = "Hello!";
        std::cout << name << "\n";
    }

    virtual ~Animals()
    {

    }

};

class Dog : public Animals
{
private:

public:
    void Voice() override
    {
        std::string name = "Woof!";
        std::cout << name << "\n";
    }

    ~Dog() override
    {
        std::cout << "Delete Dog\n";
    }

};

class Cat : public Animals
{
private:

public:
    void Voice() override
    {
        std::string name = "Meow!";
        std::cout << name << "\n";
    }

    ~Cat() override
    {
        std::cout << "Delete Cat\n";
    }

};

class Panda : public Animals
{
private:

public:
    void Voice() override
    {
        std::string name = "Chroom!";
        std::cout << name << "\n";
    }

    ~Panda() override
    {
        std::cout << "Delete Panda\n";
    }

};

int main()
{
    Animals* test = new Animals;
    test->Voice();

    Animals* p1 = new Dog;

    Animals* p2 = new Cat;

    Animals* p3 = new Panda;

    Animals* array[3]{ p1, p2, p3 };

    for (int i = 0; i < 3; i++)
    {
        array[i]->Voice();
        delete array[i];
    }

}

